package com.ignacioWolf.Base;

/**
Clase cliente a la que haremos referencia en los xml
 */
public class Cliente {
    private String nombre;
    private String apellidos;
    private String dni;

    /**
     * Constructor para rellenar por defecto los campos de Cliente
     * @param nombre El nombre del cliente.
     * @param apellidos los apellidos del cliente.
     * @param dni el dni del cliente.
     */
    public Cliente(String nombre, String apellidos, String dni) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.dni = dni;
    }
    /**
     * Constructor para rellenar por defecto los campos de Cliente (Vacíos)
     */
    public Cliente() {
    }

    /**
     * Método toString que muestra todos los clientes
     * @return String Cadena de clientes
     */
    @Override
    public String toString() {
        return "Cliente(" + nombre +", "+ apellidos+", "+ dni + ") \t";
    }


    /**
     * Getter nombre
     * @return el nombre del cliente
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Setter de nombre
     * @param nombre el nombre del cliente.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Getter apellidos
     * @return los apellidos del cliente
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Setter de apellidos
     * @param apellidos el apellido del cliente.
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Getter del dni
     * @return el dni del cliente.
     */
    public String getDni() {
        return dni;
    }
    /**
     * Método getter que devuelve el DNI del
     * @param dni el DNI del cliente.
     */
    public void setDni(String dni) {
        this.dni = dni;
    }
}
