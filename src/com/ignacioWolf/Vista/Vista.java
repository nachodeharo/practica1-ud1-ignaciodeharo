package com.ignacioWolf.Vista;
/**
 * Trabajo de exportación e importación de XMLs, con respecto a la temática individual.
 * Mi aportación personal es:
 * - Añadido un nuevo Jlabel para el DNI, el cual será unico y no se podrá repetir en todo la lista
 * - Añadido comprobante para que no se pueda insertar ningún valor en blanco
 * - Añadidos avisos de ventana para distintas fases del error.
 * - Añadido el tipo de archivo a la hora de exportar e importar.
 * @author Nacho De Haro / SrWolfMoon
 * @see Base.Cliente
 * @see Vista.Vista.Vista
 * @version 17/11/2020   1.0
 * @since 1.8
 *
 */
import com.ignacioWolf.Base.Cliente;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.LinkedList;

/**
 *Clase Vista, donde tendremos todas las operaciones y comprobaciones correspondientes de la interfaz.
 */
public class Vista {
    private JFrame frame;
    private JPanel panel1;
    private JButton altaClienteButton;
    private JButton mostrarClienteButton;
    private JComboBox comboBoxMain;
    private JTextField txtNombre;
    private JTextField txtApellidos;
    private JTextField txtDNI;
    private JLabel labelCliente;
    private JButton vaciarSelectorButton;

    private LinkedList<Cliente> lista;
    private DefaultComboBoxModel<Cliente> defaultCombo;

    /**
     * Constructor de la clase Vista, donde al ejecutarse se crean los diferentes listeners de botones (Alta, Mostrar y
     * Vaciar los clientes)
     */
    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        defaultCombo = new DefaultComboBoxModel<>();
        comboBoxMain.setModel(defaultCombo);

        /**
         * Crea un listener a la hora de pulsar el botón de "altaCliente"
         * Dentro del listener está la comprobación de si existe o no el DNI en el propio xml seleccionado y que estén
         * todos los campos rellenados
         */
        altaClienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //dar de alta cliente
                boolean comprobante = false;
                comprobante = comprobarDNI(txtDNI.getText());
                if (!txtNombre.getText().equalsIgnoreCase("") && !txtApellidos.getText().equalsIgnoreCase("") &&
                !txtDNI.getText().equalsIgnoreCase("")) {
                    if (comprobante) {
                        altaCliente(txtNombre.getText(), txtApellidos.getText(), txtDNI.getText());
                        //listar clientes en el combobox
                        refrescarComboBox();
                    } else {
                        JOptionPane.showMessageDialog(null, "YA EXISTE EL DNI: " + txtDNI.getText(),
                                "INFO", JOptionPane.INFORMATION_MESSAGE);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "NO PUEDE ESTAR NIGÚN CAMPO EN BLANCO",
                            "INFO", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        /**
         * Crea un listener a la hora de pulsar el botón de "mostrarCliente"
         * Dentro del listener se seleccionará el cliente del comboBox y se mostrará en la interfaz.
         */
        mostrarClienteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // muestro el cliente seleccionado
                try {
                    Cliente clienteSeleccionado = (Cliente) defaultCombo.getSelectedItem();
                    labelCliente.setText(clienteSeleccionado.toString());
                }catch (Exception error){
                    error.printStackTrace();
                }
            }
        });

        /**
         * Crea un listener a la hora de pulsar el botón de "VaciarSelector"
         * Al hacer click se vaciará el comboBox para poder crear uno de 0.
         */
        vaciarSelectorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                defaultCombo.removeAllElements();
                lista.clear();
                txtNombre.setText("");
                txtApellidos.setText("");
                txtDNI.setText("");
            }
        });
    }

    /**
     * Comprueba un dni introducido por parametro para evitar la duplicidad de DNI
     * @param dni El dni a comprobar
     * @return boolean false si no se puede crear el cliente por que ya existe el DNI y true si se pudiera.
     */
    private boolean comprobarDNI(String dni){

        for(Cliente cliente:lista){
            if(cliente.getDni().equalsIgnoreCase(dni)){
                return false;
            }
        }
        return true;
    }

    /**
     * Da de alta un cliente según la clase Cliente y la añade a la lista
     * @param nombre nombre del cliente
     * @param apellido apellidos del cliente
     * @param dni dni del cliente
     */
    private void altaCliente(String nombre, String apellido, String dni) {
        Cliente cliente = new Cliente(nombre,apellido,dni);
        lista.add(cliente);
    }

    /**
     * Crea el menú "archivo" para poder exportar e importar los archivos XML
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");

        /**
         * Crea un listener para poder exportar los datos a XML
         */
        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                FileNameExtensionFilter extFilter = new FileNameExtensionFilter("Archivo XML", "xml");
                selectorArchivo.addChoosableFileFilter(extFilter);
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarXML(fichero);
                }

            }
        });

        /**
         * Crea un listener para poder importar los datos de un XML a la interfaz (y añadirlos al combobox)
         */
        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                FileNameExtensionFilter extFilter = new FileNameExtensionFilter("Archivo XML", "xml");
                selectorArchivo.addChoosableFileFilter(extFilter);
                /*
                FileNameExtensionFilter extFilter = new FileNameExtensionFilter("Archivo XML", "xml");
                selectorArchivo.addChoosableFileFilter(extFilter);
                selectorArchivo.showOpenDialog(null);

                 */
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    /**
     * Importa la información de un xml a un comboBox y a una lista
     * @param fichero El fichero seleccionado
     */
    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro todos los nodos para mostrar clientes
            NodeList clientes = documento.getElementsByTagName("cliente");
            for (int i = 0; i < clientes.getLength(); i++) {
                Node cliente = clientes.item(i);
                Element elemento = (Element) cliente;

                //Obtengo los campos marca y modelo
                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String apellidos = elemento.getElementsByTagName("apellidos").item(0).getChildNodes().item(0).getNodeValue();
                String dni = elemento.getElementsByTagName("dni").item(0).getChildNodes().item(0).getNodeValue();

                altaCliente(nombre, apellidos, dni);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    /**
     * Exporta la información de un comboBox a un archivo XML
     * @param fichero
     */
    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz y procedo a bajar al hijo
            Element raiz = documento.createElement("clientes");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoCliente;
            Element nodoDatos;
            Text dato;

            //Por cada cliente de la lista, creo un nodo de cliente
            for (Cliente cliente : lista) {

                //Creo un nodo cliente y lo añado al nodo raiz (clientes)
                nodoCliente = documento.createElement("cliente");
                raiz.appendChild(nodoCliente);

                //A cada nodo cliente le añado los nodos nombre, apellidos y dni
                nodoDatos = documento.createElement("nombre");
                nodoCliente.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(cliente.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("apellidos");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(cliente.getApellidos());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("dni");
                nodoCliente.appendChild(nodoDatos);

                dato = documento.createTextNode(cliente.getDni());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un fichero de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Clase main del programa, donde se llama a la vista.
     * @param args de java
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    /**
     * Refresca el comboBox para cuando se importa un xml se haya eliminado todo lo anterior también.
     */
    private void refrescarComboBox() {
        defaultCombo.removeAllElements();
        for (Cliente cliente : lista) {
            defaultCombo.addElement(cliente);
        }
    }
}
